/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.util.Scanner;

/**
 *
 * @author Itzik fatal
 */
public class Players {
    String mPlayerID;
    String mPassword;
    String mFirstName;
    String mLastName;
    String mEmail;
    
    //function that accepts values
    Players(String mPlayerID,String mPassword,String mFirstName,String mLastName,String mEmail){
        this.mPlayerID=mPlayerID;
        this.mPassword=mPassword;
        this.mFirstName=mFirstName;
        this.mLastName=mLastName;
        this.mEmail=mEmail;
    }
    
    //function that receives values
    Players(){
        Scanner textscan=new Scanner(System.in);
        System.out.println("enter mPlayerID:");
        mPlayerID=textscan.nextLine();
        System.out.println("enter mPassword:");
        mPassword=textscan.nextLine();
        System.out.println("enter mFirstName:");
        mFirstName=textscan.nextLine();
        System.out.println("enter mLastName:");
        mLastName=textscan.nextLine();
        System.out.println("enter mEmail:");
        mEmail=textscan.nextLine();
    }
    
    public String getmPlayerID() // get for mPlayerID --> UserName
    {
        return mPlayerID;
    }
    public void setmPlayerID(String value) // set for mPlayerID
    {
      mPlayerID= value;  
    }
    
    public String getmPassword() // get for mPassword
    {
        return mPassword;
    }
    public void setmPassword(String value) // set for mPassword
    {
      mPassword= value;  
    }
    
    public String getmFirstName() // get for mFirstName
    {
        return mFirstName;
    }
    public void setmFirstName(String value) // set for mFirstName
    {
      mFirstName= value;  
    }
    
    public String getmLastName() // get for mLastName
    {
        return mLastName;
    }
    public void setmLastName(String value) // set for mLastName
    {
      mLastName= value;  
    }
    
    public String getmEmail() // get for mEmail
    {
        return mEmail;
    }
    public void setmEmail(String value) // set for mEmail
    {
      mEmail= value;  
    }
}
