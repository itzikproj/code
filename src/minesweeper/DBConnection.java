/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.sql.*;
import java.util.List;

/**
 *
 * @author Itzik fatal
 */
public class DBConnection {
        String userName = "Itzik";
        String password = "9218";
        String dbName = "MineSweeperDB";  // המקורי
        //String dbName = "Scarbble";
    
    // fanction to get a link to database 
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        String url = "jdbc:sqlserver://LAPTOP-08JJ3VRL;databaseName="+dbName ;
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Connection conn;
        conn = DriverManager.getConnection(url, userName, password);
        return conn;
    }
    
    public ResultSet getResulset(String sql) throws Exception {
        Connection conn = getConnection();
        Statement st;
        st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        return rs;
    }
    
    //Using Prepared Statements
    //https://docs.oracle.com/javase/tutorial/jdbc/basics/prepared.html
    
    public void insertPlayers(Players Player) throws SQLException, Exception {
        Connection conn = null;
        PreparedStatement stInsertPlayer = null;
        String insertString = 
            "INSERT INTO [" + dbName + "].[dbo].[Players]\n" 
            + "  ([PlayerID]" 
            + ",  [Password]" 
            + ",  [FirstName]" 
            + ",  [Lastname]" 
            + ",  [Email])" 
            + " VALUES (?,?,?,?,?)";
            try {
               conn  = getConnection();
               stInsertPlayer = conn.prepareStatement(insertString);
               stInsertPlayer.setString(1, Player.getmPlayerID());
               stInsertPlayer.setString(2, Player.getmPassword());
               stInsertPlayer.setString(3, Player.getmFirstName());
               stInsertPlayer.setString(4, Player.getmLastName());
               stInsertPlayer.setString(5, Player.getmEmail());
               stInsertPlayer.executeUpdate();
        }
            catch(ClassNotFoundException | SQLException sqle){
            System.out.println("Close Not Found Exception " + sqle.toString());
        }finally {
            if (stInsertPlayer != null){  // null כי אם נפלנו בהתחלה זה יהיה
                stInsertPlayer.close();
            }
            if (conn != null){  // לדוגמה אם נפל החשמל
                if (!conn.isClosed()){
                    conn.close();
                }
            }  
        }
    }
    public void updatePlayers(Players Player) throws SQLException, Exception {
        Connection conn = null;
        PreparedStatement stUpdatePlayer = null;
        String updateString =
            "update [" + dbName + "].[dbo].[Players]"
                + " set [Password] = ?"
                + ",    [FirstName] = ? "
                + ",    [LastName] = ? "
                + ",    [Email] = ?"
                + " where [PlayerID] = ?"; 
        try {
            conn  = getConnection();
            stUpdatePlayer = conn.prepareStatement(updateString);
                stUpdatePlayer.setString(1, Player.getmPassword());
                stUpdatePlayer.setString(2, Player.getmFirstName());
                stUpdatePlayer.setString(3, Player.getmLastName());
                stUpdatePlayer.setString(4, Player.getmEmail());
                stUpdatePlayer.setString(5, Player.getmPlayerID()); 
                stUpdatePlayer.executeUpdate();
        }
        catch(ClassNotFoundException | SQLException sqle){
            System.out.println("Close Not Found Exception " + sqle.toString());
        }
        finally {
            if (stUpdatePlayer != null){  // null כי אם נפלנו בהתחלה זה יהיה
                stUpdatePlayer.close();
            }
            if (conn != null){  // לדוגמה אם נפל החשמל
                if (!conn.isClosed()){
                    conn.close();
                }
            }  
        }
    }
}
